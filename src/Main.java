import com.sun.org.apache.regexp.internal.RE;

import java.util.Optional;
import java.util.Scanner;

/**
 * Created by amen on 8/12/17.
 */
public class Main {
    public static void main(String[] args) {
        // TODO: parse request
        Scanner sc = new Scanner(System.in);
        WebService service = new WebService();

        String line = null;
        do {
            line = sc.nextLine();
            Optional<Request> newRequest = RequestParser.parseRequest(line);
            newRequest.ifPresent((req)->{
                System.out.println(newRequest);
                service.handleRequest(newRequest.get());
            });
//            if (newRequest.isPresent()) {
//                System.out.println(newRequest);
//                service.handleRequest(newRequest.get());
//            } else {
//                System.out.println("Couldn't parse that request.");
//            }
        } while (!line.equals("quit"));
    }
}
